package Main;

import Algorithm.DijkstraAlgorithm;
import org.graphstream.algorithm.Dijkstra;
import org.graphstream.stream.file.FileSourceFactory;

import java.io.File;
import java.io.FileWriter;



public class Main {
    public static void main(String[] args) throws  Exception {

        int number_of_Node = 5 ;
        GraphDisplay graphTest = new GraphDisplay("CompareGraph", number_of_Node);

        System.out.printf("N: %6d=======================================================%n" , number_of_Node);
        //affichage de plus court chemin entre les noeuds Dijkstra GraphStream
        Dijkstra dijkstra1 = graphTest.setupGSDijkstraAlgoirthm() ;
        dijkstra1.compute();
        graphTest.PrintPathLengthFromDijkstra(dijkstra1);


        System.out.println("______________________________________________________________________________");

        //affichage de plus court chemin algorithme implementé
        DijkstraAlgorithm dijkstraAlgorithm1 = graphTest.setupDijkstraAlgoirthm() ;
        dijkstraAlgorithm1.compute();
        graphTest.PrintPathLengthFromDijkstra2(dijkstraAlgorithm1);



        for( int Number_of_Nodes = 10000; Number_of_Nodes < 100000;  Number_of_Nodes+=10000) {
            GraphDisplay graphDisplay = new GraphDisplay("Graph", Number_of_Nodes);

            double StartTime1, EndTime1, StartTime2, EndTime2;
            System.out.printf("N: %6d===================================================%n", Number_of_Nodes);

            // initialisation et exécution de l'algorithme
            DijkstraAlgorithm dijkstraAlgorithm = graphDisplay.setupDijkstraAlgoirthm();

            StartTime1 = System.currentTimeMillis();
            dijkstraAlgorithm.compute();
            EndTime1 = System.currentTimeMillis();




            System.out.printf("Algorithm de Dijkstra (mon Implementation) execute en %.8fms%n", EndTime1 - StartTime1);
            //initialisation et exécution de l'algorithme de GraohStream
            Dijkstra dijkstra = graphDisplay.setupGSDijkstraAlgoirthm();

            StartTime2 = System.currentTimeMillis();
            dijkstra.compute();
            EndTime2 = System.currentTimeMillis();


            System.out.printf("Algorithm de Dijkstra (GraphStream) execute en %.8fms%n", EndTime2 - StartTime2);

        }
    }
}
