package Main;

import Algorithm.DijkstraAlgorithm;
import GraphGenerator.GraphGenerator;
import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.Random;

public class GraphDisplay {

    /**
     * le graphe qu'on va utiliser pour notre algorithme
     */
    private Graph graph;


    /**
     * le noeud de départ pour notre algorithme
     */
    private Node start;

    /**
     * Constructeur
     *
     * @param GraphName:       le nom de graphe qu'on va utiliser dans notre algorithme
     * @param Number_of_Nodes: le nombre des noeuds à générer aléatoirement à l'aide de la classe
     *                         GraphGenerator
     */
    public GraphDisplay(String GraphName, int Number_of_Nodes) {
        this.graph = new GraphGenerator().create(GraphName, Number_of_Nodes);

        // on choisit un départ aléatoirement tant qu'on tombe sur un noeud n'ayant aucune arête sortante
        do {
            start = graph.getNode(getRandomNodeId());
        } while (start.leavingEdges().findAny().isEmpty());
    }

    /**
     * Cette méthode permet de renvoyer le nom d'un noeud aléatoirement
     *
     * @return Une chaîne de carectère contenant le nomd  du noeud
     */
    public String getRandomNodeId() {
        Random random = new Random(System.currentTimeMillis());
        return graph.getNode(random.nextInt(graph.getNodeCount())).getId();
    }

    /**
     * Méthode permet de afficher le graphe
     */
    public void display() {
        System.setProperty("org.graphstream.ui", "swing");
        this.graph.display();
    }

    /**
     *
     * @return le noeud de départ
     */
    public Node getStart() {

        return start;
    }

    /**
     *
     * @return le graphe généré
     */
    public Graph getGraph() {
        return graph;
    }


    /**
     * Méthode pour initialiser l'agorithme de Dijkstra
     *
     * @return : un objet DijkstraAlgorithm avec une initialisation de graphe
     */
    public DijkstraAlgorithm setupDijkstraAlgoirthm() {
        return new DijkstraAlgorithm(this.graph, (String) this.start.getAttribute("name"));
    }

    /**
     * cette méthode permet d'afficher le graphe implimenté par l'algorithme dijkstra de GrapheStream
     * @param dijkstra: algorithme djikstra de grapheStream
     */
    public void PrintPathLengthFromDijkstra(Dijkstra dijkstra){
        this.graph.nodes().forEach(node ->
                        System.out.printf("\"%s->%s:%10.2f%n\"" , dijkstra.getSource() , node, dijkstra.getPathLength(node))
                );
    }

    /**
     * cette méthode permet d'afficher le plus court chemin entre les noeudss
     * @param dijkstraAlgorithm : implémentation naîve de l'algorithme
     */
    public void PrintPathLengthFromDijkstra2(DijkstraAlgorithm dijkstraAlgorithm){
       for(Node node : this.graph){
           Integer result = (Integer) dijkstraAlgorithm.getPathsLength(node) ;
           String path = String.valueOf(result) ;
           if(result.equals(Integer.MAX_VALUE) )
               path = "Infinity" ;
           System.out.printf("%s->%s: %10s%n", dijkstraAlgorithm.getStart(), node.getAttribute("name"), path
                   );
       }
    }

    /**
     * Méthode pour initialiser l'agorithme de Dijkstra (GraphStream)
     *
     * @return un objet dijkstra avec un graphe initialisé
     */

    public Dijkstra setupGSDijkstraAlgoirthm() {
        Dijkstra dijkstra = new Dijkstra(Dijkstra.Element.EDGE, "dist", "weight");
        dijkstra.init(this.graph);
        dijkstra.setSource(this.start);
        return dijkstra;
    }



}
