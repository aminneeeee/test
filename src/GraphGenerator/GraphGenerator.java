package GraphGenerator;

import java.util.Random;

import org.graphstream.algorithm.generator.RandomGenerator;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;

/**
 * Cette Classe Permet de générer aléatoirement des graphe
 */
public class GraphGenerator {
    /**
     * Générateur de graphe
     * <p>
     * Note: On utilise arbitrairement un degré moyen de 4
     */
    private final RandomGenerator generator;

    /**
     * un objet random permettant de génénrer des valeurs aléatoire
     */
    private final Random random;
    /**
     * Paramètre indiquant la valeur numérique du prochain nom à donner à un Noeud
     */
    private int nextChar;

    /**
     * Initailisation des variables privés
     */
    public GraphGenerator() {
        this.generator = new RandomGenerator(4, true);
        this.random = new Random(System.currentTimeMillis());
        this.nextChar = 0;
    }

    /**
     * Méthode permet de générer un graphe aléatoirement
     *
     * @param graphName Nom du graphe
     * @param nodeCount Nombre de noeuds à mettre dans le graphe
     * @return le grahpe générer
     */
    public Graph create(String graphName, int nodeCount) {
        Graph graph = new SingleGraph(graphName);

        generator.addSink(graph);
        generator.setRandomSeed(System.currentTimeMillis());
        generator.setDirectedEdges(true, true);

        // début de la génération du graphe
        generator.begin();

        // on ajoute nodeCount noueds au graphe
        for (int i = 0; i < nodeCount; i++)
            generator.nextEvents();

        // fin de la génération
        generator.end();

        // On supprime aléatoirement les noeuds  qui étaient générés dans la méthode begin
        while (graph.getNodeCount() > nodeCount)
            graph.removeNode(random.nextInt(graph.getNodeCount()));

        // assignation de poids aléatoire à chaque arête
        graph.edges().forEach(edge -> {
            int weight = random.nextInt(10) + 1;
            edge.setAttribute("weight", weight);
            edge.setAttribute("ui.label", weight);
        });

        // Génération et assignation de noms à chauqe noeud
        graph.nodes().forEach(node -> {
            String name = getRandomName();
            node.setAttribute("name", name);
            node.setAttribute("ui.label", name);
            node.setAttribute("ui.style", "text-alignment: at-right ;");
        });

        // on renvoie le graphe générer
        return graph;
    }

    /**
     * Méthode permettant de générer un nom auto-incrémenté pour un noeud
     * <p>
     * Note : cette  methode permet de geénérer les noms Suivants : A,B,C,....Z, AA,AB,AC....
     * on totale on a une possibilité de Integer.MAX_Value nom possible
     *
     * @return Une String contenant le nom du noeud
     */
    private String getRandomName() {
        // utilisation de la calsse StringBuilder pour créer une chaîne de caractère
        StringBuilder stringBuilder = new StringBuilder();
        // on incrémente l'entier nextChar
        int n = ++nextChar;
        while (n > 0) {
            //on récupère la position de l'alphabet en utilisant l'opération modulo
            int mod = n % 26;

            // si mod == 0, cela veut dire qu'il s'agit de la dernière lettre de l'alphabet.
            if (mod == 0) {
                // on ajoute Z comme c'est la dernière  lettre
                stringBuilder.append("Z");
                // division entière de n par 26 et soustraction de 1 pour revenir à  0
                n = (n / 26) - 1;
            } else {
                // on ajoute la lettre de l'alphabet suivant le tableau ASCII en commençant par A et on ajoute (mod - 1)
                // n = 1 => mod = 1 =>  (mod -1) + A => A
                // n = 2 => mod = 2 =>  (mod -1) + A => 1 + A ==> B (A = 65 (table Ascii)  et B = 66 (table ASCII))
                stringBuilder.append((char) ((mod - 1) + 'A'));
                // on divise par 26
                n = n / 26;
            }
        }

        return stringBuilder.toString();
    }
}
