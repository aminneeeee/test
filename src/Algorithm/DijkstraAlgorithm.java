package Algorithm;

import org.graphstream.algorithm.Algorithm;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Implémentation de l'algorithme de Dijkstra héritant de la classe Algorithm de GraphStream
 */
public class DijkstraAlgorithm implements Algorithm {
    /**
     * le graphe qu'on va utiliser pour tester notre algorithm
     */
    private Graph graph;

    /**
     * une chaîne de caractère qui représente le noeud de départ de l'algorithm
     */
    private final String Start;


    /**
     * Constructeur de notre calsse
     *
     * @param graph : le graphe qui sera utilisé pour tester l'algorithme implémenté.
     * @param start : une chaîne de caractère qui représente le noeud de départ de l'algorithm
     */
    public DijkstraAlgorithm(Graph graph, String start) {
        Start = start;
        init(graph);
    }


    /**
     * redifinition de la méthode init de l'interface algorithme
     * Initialisation de l'algorithme
     *
     * @param graph : le graphe qui sera utilisé pour tester l'algorithme implémenté.
     */
    @Override
    public void init(Graph graph) {
        this.graph = graph;
    }

    /**
     * cette méthode permet de retrouver un noeud à partitr d'un nom
     *
     * @param name : le nom de noeud à charcher
     * @return le premier noeud trouver dans l'ensemble des noeuds
     */
    private Node getByName(String name) {
        //on renvoie le resultat en utilisant les Streams
        return graph.nodes().filter(node -> node.getAttribute("name").equals(name)).findFirst().orElse(null);
    }

    public String getStart() {
        return Start;
    }



    /**
     * cette méthode permet de retourner le poids d'une arête
     *
     * @param edge : L'arête utilitsée pour trouver le poids
     * @return un entier qui represente le poids d'une arête
     */
    private int weight(Edge edge) {
        // utilisation de la méthode getAttribute du GraphStream
        return (int) edge.getAttribute("weight");
    }

    /***
     * cette méthode va nous permettre de renvoyer l'attribut dist
     * @param node le noeud duquel on veut évaluer l'attribut "dist"
     * @return la valeur de l'attribut dist
     */
    private int dist(Node node) {
        //utilisation de la méthode getAttribute de GraphStream
        return (int) node.getAttribute("dist");
    }


    @Override
    /*
     * redifinition de la methode compute de GraphStream
     */
    public void compute() {
        //Initialisation d'une file
        Queue<Node> queue = new ArrayDeque<>();
        // Récupération du premier noeud à l'aide de la fonction getByName
        // L'algorithme va commencer à explorer tous les autres chemins à partir de ce noeud.
        Node s = getByName(Start);

        //Initialisez l'attribut dist en -1 pour tous les nœuds sauf le nœud de départ.
        // dans le nœud de départ, l'attribut dist sera initialisé à 0.
        graph.nodes().forEach(node -> {
            if (!node.getAttribute("name").equals(Start)) {
                node.setAttribute("dist", Integer.MAX_VALUE);
                node.setAttribute("parent", (Object) null);
            } else {
                // noeud de départ
                node.setAttribute("dist", 0);
            }
        });

        // on ajoute le sommet s à la file
        queue.add(s);

        //Tant qu'on a des noeuds dans la file, on choisit celui avec le coût est minimal
        while (!queue.isEmpty()) {
            Node u = extractMin(queue);
            queue.remove(u);

            // pour toutes les arêtes sortantes de u on va trouver les voisin ensuite on va mettre à jour l'attribut dist
            u.leavingEdges().forEach(edge -> {
                Node v = edge.getOpposite(u);
                if (dist(v) > dist(u) + weight(edge)) {
                    v.setAttribute("dist", dist(u) + weight(edge));
                    // ajouter le sommet u à l'attribut parent de chauque sommet voisin de u
                    v.setAttribute("parent", u.getAttribute("name"));
                    // on ajoute le sommet v à la file
                    queue.add(v);

                }
            });
        }


    }

    /**
     * cette méthode permet d'extraire le noeud de coût minimal dans la file
     *
     * @param queue une file qui contient les sommets de graphe
     * @return le noeud de coût de minimal
     */
    private Node extractMin(Queue<Node> queue) {
        if (queue.size() <= 1) return queue.poll();
        Node minNode = null;
        int min = -1;
        for (Node node : queue) {
            int nDist = dist(node);
            if (min == -1 || nDist < min) {
                min = nDist;
                minNode = node;
            }
        }
        return minNode;
    }


    /***
     * cette méthode va nous permettre de renvoyer l'attribut dist
     * @param node le noeud duquel on veut évaluer l'attribut "dist"
     * @return la valeur de l'attribut dist en format d'un objet
     */
    public Object getPathsLength(Node node) {
        return node.getAttribute("dist");
    }

}
